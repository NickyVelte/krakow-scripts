#!/usr/bin/python
# coding: utf-8
# Autologin script for multiple accounts

import datetime
import sys  # sys.stdout.write('.')
import traceback
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC

import csv

phantom = webdriver.PhantomJS()
logfile = open('login-bot.log', 'a')
logfile.write('['+datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+']: run login-bot\n')

forumurl = 'http://holerajasna.liverolka.ru/login.php'

def login(u, p):
    logfile.write(u + ':')
    try:
        phantom.get(forumurl)
        try:
            username = WebDriverWait(phantom,10).until(EC.presence_of_element_located((By.XPATH,'//input[@size = "25"]')))
            password = WebDriverWait(phantom,10).until(EC.presence_of_element_located((By.XPATH,'//input[@size = "16"]')))
            try:
                username.send_keys(u.decode('utf-8'))
                password.send_keys(p.decode('utf-8'), Keys.RETURN)
                phantom.implicitly_wait(3)
            except Exception:
                pass
        except Exception:
            logfile.write('\tlogin form not found\n')
    except Exception:
        pass

def logout():
    try:
        # phantom.find_element_by_xpath('//li[@id ="navlogout"]/a/span/img').click()
        logout = WebDriverWait(phantom,10).until(EC.presence_of_element_located((By.XPATH,'//li[@id="navlogout"]/a/span/img')))
        logout.click()
        logfile.write('\tOK\n')
    except Exception:
        logfile.write('\tERROR on logout.\n')

def main():
    try:
        # print '\tЛогин-бот: старт'
        with open('data.csv', 'rU') as data:
            reader = csv.DictReader(data)
            for row in reader:
                login(row['usr'], row['pass'])
                logout()
        phantom.quit()
        # print '\tЛогин-бот: готово!'
    except KeyboardInterrupt:
        print '\nShutdown requested...exiting'
    except Exception:
        traceback.print_exc(file=sys.stdout)
    sys.exit(0)

if __name__ == "__main__":
    main()
    try:
        phantom.quit()
    except Exception:
        pass
